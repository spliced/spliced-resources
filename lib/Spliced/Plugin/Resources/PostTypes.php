<?php
namespace Spliced\Plugin\Resources;

/**
 * Class Spliced\Plugin\Resources\PostTypes
 */
class PostTypes {
	static $hooker;
	const HOOK_PREFIX = 'spliced-resources';
	const RESOURCE = 'spliced_resource';

	static public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \Spliced\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, self::HOOK_PREFIX );
	}

	static public function action_init() {
		$args = array(
			'label' => _x( 'Resources', 'plural post type label', 'spliced-resources' ),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array(
				'slug' => _x( 'resources', 'post type rewrite slug', 'spliced-resources' )
			),
		);

		register_post_type( self::RESOURCE, $args );
	}
}