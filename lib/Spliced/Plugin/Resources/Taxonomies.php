<?php
namespace Spliced\Plugin\Resources;

/**
 * Class Spliced\Plugin\Resources\Taxonomies
 */
class Taxonomies {
	static $hooker;
	const HOOK_PREFIX = 'spliced-resources';
	const RESOURCE_TAG = 'spliced_resource_tag';
	const RESOURCE_AUDIENCE = 'spliced_resource_audience';
	const RESOURCE_TYPE = 'spliced_resource_type';

	static public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \Spliced\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, self::HOOK_PREFIX );
	}

	static public function action_init() {
		$args = array(
			'label' => _x( 'Audiences', 'plural taxonomy label', 'spliced-resources' ),
			'public' => true,
			'rewrite' => array( 'slug' => _x( 'audience', 'taxonomy rewrite slug', 'spliced-resources' ) ),
			'meta_box_cb' => 'post_categories_meta_box',
			'hierarchical' => true,
		);

		register_taxonomy( self::RESOURCE_AUDIENCE,  PostTypes::RESOURCE, $args );

		$args = array(
			'label' => _x( 'Tags', 'plural taxonomy label', 'spliced-resources' ),
			'public' => true,
			'rewrite' => array( 'slug' => _x( 'resources-tag', 'taxonomy rewrite slug', 'spliced-resources' ) ),
		);

		register_taxonomy( self::RESOURCE_TAG,  PostTypes::RESOURCE, $args );

		$args = array(
			'label' => _x( 'Types', 'plural taxonomy label', 'spliced-resources' ),
			'public' => true,
			'rewrite' => array( 'slug' => _x( 'resources-type', 'taxonomy rewrite slug', 'spliced-resources' ) ),
		);

		register_taxonomy( self::RESOURCE_TYPE,  PostTypes::RESOURCE, $args );
	}
}