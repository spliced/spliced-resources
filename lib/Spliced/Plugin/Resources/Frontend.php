<?php
namespace Spliced\Plugin\Resources;

/**
 * Class Spliced\Plugin\Resources\Frontend
 */
class Frontend {
	static $hooker;

	static public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \Spliced\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	static public function action_pre_get_posts( &$query ) {
		if ( $query->is_tax( array( Taxonomies::RESOURCE_AUDIENCE, Taxonomies::RESOURCE_TAG, Taxonomies::RESOURCE_TYPE ) ) ) {
			$query->set( 'post_type', PostTypes::RESOURCE );
		}
	}

	static public function get_download_url( $post_id ) {
		$attachment_id = get_post_meta( $post_id, 'spliced_resources_file', true );
		$url           = wp_get_attachment_url( $attachment_id );

		if ( ! empty( $url ) ) {
			return esc_url( $url );
		}
	}

	static public function get_external_url( $post_id ) {
		$url = get_post_meta( $post_id, 'spliced_resources_url', true );

		if ( ! empty( $url ) ) {
			return esc_url( $url );
		}
	}
}