<?php
namespace Spliced\Plugin\Resources;

/**
 * Class Spliced\Plugin\Resources\Core
 */
class Core {
	static $hooker;
	const HOOK_PREFIX = 'spliced-resources';

	static public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \Spliced\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, self::HOOK_PREFIX );

		PostTypes::bootstrap( $hooker );
		Taxonomies::bootstrap( $hooker );

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );

		if ( is_admin() ) {
			Admin::bootstrap( $hooker );
			define( 'CMB_URL', '/vendor/humanmade/Custom-Meta-Boxes/' );
			require_once ABSPATH . '../vendor/humanmade/Custom-Meta-Boxes/custom-meta-boxes.php';
		} else {
			Frontend::bootstrap( $hooker );
		}
	}

	static public function activation() {
		flush_rewrite_rules( true );
	}

	static public function action_init() {
		static $wp_hook_priority = 12;
		$resource_slug = get_post_type_object( PostTypes::RESOURCE )->rewrite['slug'];

		add_rewrite_rule(
			"^$resource_slug/" . get_taxonomy( Taxonomies::RESOURCE_AUDIENCE )->rewrite['slug'] . '/([^/]*)/?',
			'index.php?' . build_query(
				array(
					Taxonomies::RESOURCE_AUDIENCE => '$matches[1]',
				)
			),
			'top'
		);

		add_rewrite_rule(
			"^$resource_slug/" . __( 'tag', 'numberpartners' ) .  '/([^/]*)/?',
			'index.php?' . build_query(
				array(
					Taxonomies::RESOURCE_TAG => '$matches[1]',
				)
			),
			'top'
		);

		// flush_rewrite_rules();
	}

	static public function filter_term_link( $termlink, $term, $taxonomy ) {
		$resource_slug = get_post_type_object( PostTypes::RESOURCE )->rewrite['slug'];

		if ( Taxonomies::RESOURCE_AUDIENCE === $taxonomy ) {
			return site_url( "$resource_slug/" . get_taxonomy( Taxonomies::RESOURCE_AUDIENCE )->rewrite['slug'] . '/' . $term->slug );
		}

		if ( Taxonomies::RESOURCE_TAG === $taxonomy ) {
			return site_url( "$resource_slug/" . __( 'tag', 'numberpartners' )  . '/' . $term->slug );
		}
		return $termlink;
	}

	static public function get_resource_attachment( $post = null ) {
		$post = get_post( $post );

		$children = get_children(
			array(
				'post_type' => 'attachment',
				'post_parent' => $post->ID,
				'post_status' => 'publish,inherit',
				'orderby' => 'menu_order, date',
			)
		);

		return array_shift( $children );
	}

	static public function get_resource_file_extension( $attachment ) {
		$pathinfo = pathinfo( get_attached_file( $attachment->ID ) );

		return $pathinfo['extension'];
	}
}