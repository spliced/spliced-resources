<?php
namespace Spliced\Plugin\Resources;

/**
 * Class Spliced\Plugin\Resources\Admin
 */
class Admin {
	static $hooker;

	static public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \Spliced\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	static public function filter_cmb_meta_boxes( $meta_boxes ) {
		$prefix = 'spliced_resources_'; // Prefix for all fields

		$fields[] = array(
			'name' => __( 'File', 'spliced-resources' ),
			'id' => $prefix . 'file',
			'type' => 'file',
			'desc' => __( 'Add a file for this resource' ),
		);

		$fields[] = array(
			'name' => __( 'External URL', 'spliced-resources' ),
			'id' => $prefix . 'url',
			'type' => 'text_url',
			'desc' => __( 'If this resource is hosted off site (eg. YouTube) enter the full URL' ),
		);

		$meta_boxes['business_address'] = array(
			'id' => 'business_address',
			'title' => __( 'Resource', 'geleiacultural' ),
			'pages' => array( PostTypes::RESOURCE ), // post type
			'context' => 'normal',
			'priority' => 'high',
			'fields' => $fields,
		);

		return $meta_boxes;
	}
}